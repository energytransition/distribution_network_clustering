from pylatex import Section, Subsection, Command, Document, PageStyle, Head, MiniPage, Foot, LargeText, \
    MediumText, LineBreak, simple_page_number, escape_latex, Package, Figure, Table, Tabular,  NewLine, NewPage
from pylatex.section import Chapter
from pylatex.utils import italic, NoEscape
from load_network_data import load_regions
from get_network_params import group_by_substation, process_network
from cluster_network import load_params
from tqdm.contrib.concurrent import process_map
from pathlib import Path
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd

report_path = Path.cwd().parent / 'reports'
plot_path = Path.cwd().parent / 'plots'
data_path = Path.cwd().parent / 'data'


def generate_header(doc):

    # Add document header
    header = PageStyle("header")
    # Create left footer
    with header.create(Foot("L")):
        header.append("Mines Paris")
    # Create center footer
    with header.create(Foot("C")):
        header.append("Yassine Abdelouadoud")
    # Create right footer
    with header.create(Foot("R")):
        header.append(simple_page_number())

    doc.preamble.append(header)
    doc.change_document_style("header")


def fill_title_page(doc, voltage_level):
    """

    :param doc:
    :return:
    """
    v_lev = 'HTA' if voltage_level == 'mv' else 'BT'
    title = f'Classification ascendante hiérarchique appliquée aux réseaux {v_lev}'

    doc.preamble.append(Command('title', title))
    doc.preamble.append(Command('author', 'Yassine Abdelouadoud, Mines Paris'))
    doc.preamble.append(Command('date', NoEscape(r'\today')))
    doc.append(NoEscape(r'\maketitle'))


def hyperlink(url,text):
    text = escape_latex(text)
    return NoEscape(r'\href{' + url + '}{' + text + '}')


def fill_results(doc, n_clusters, medoids, network_data, voltage_level):

    distribution_plot_folder = plot_path / f"cluster_distribution_{voltage_level}_{method}_{metric}"
    network_graph_plot_folder = plot_path / f"cluster_graph_{voltage_level}_{method}_{metric}"

    with doc.create(Chapter('Résultats')):
        for i in range(n_clusters):
            cluster_label = i + 1
            medoid_df = network_data.loc[(network_data.cluster_label == cluster_label) & network_data.medoid,
                                         network_data.columns].copy()
            with doc.create(Section(f"Cluster n° {cluster_label}")):

                desc = cluster_description(network_data, cluster_label, voltage_level)
                doc.append(desc)
                medoid_table(doc, network_data, cluster_label)
                print(medoid_df.index[0])
                distribution_filename = distribution_plot_folder / f"cluster_{cluster_label}.pdf"
                network_graph_filename = network_graph_plot_folder / f"graph_{voltage_level}_{medoid_df.index[0]}.pdf"
                with doc.create(Figure(position='H')) as distribution_pic:
                    distribution_pic.add_image(distribution_filename.as_posix(), width='400px')
                    distribution_pic.add_caption('Distribution des principaux paramètres de description des réseaux du'
                                                 ' cluster (valeurs normalisées). Les valeurs prises par le réseau'
                                                 ' représentatifs sont tracées en rouge pointillé.')

                with doc.create(Figure(position='H')) as network_graph_pic:
                    network_graph_pic.add_image(network_graph_filename.as_posix(), width='260px')
                    network_graph_pic.add_caption('Graphe du réseau médoïde du cluster')

            doc.append(NoEscape(r"\pagebreak[4]"))


def cluster_description(network_data, cluster_label, voltage_level):

    v_lev = 'HTA' if voltage_level == 'mv' else 'BT'

    medoid_id = network_data.loc[(network_data.cluster_label == cluster_label) & network_data.medoid].index[0]

    network_count = network_data.loc[network_data.cluster_label == cluster_label].shape[0]
    network_length = round(network_data.loc[network_data.cluster_label == cluster_label, 'total_length'].sum())
    leaf_nodes = round(network_data.loc[network_data.cluster_label == cluster_label, 'leaf_nodes'].sum())

    return f"Le cluster {cluster_label} contient {network_count} réseaux d'une longueur totale de {network_length} kms " \
           f"et desservant {leaf_nodes} postes {v_lev}. Il est représenté par le réseau dont la sous-station porte l'ID" \
           f" {medoid_id}. Ci-dessous les paramètres de description principaux du réseau représentatif suivi de leur" \
           f" distribution pour l'ensemble des réseaux du cluster et du graphe du réseau représentatif : "


def medoid_table(doc, network_data, cluster_label):
    cols = {'aerial_share': 'Part aérien', 'mean_R_l': 'R linéique', 'mean_imax': 'I max',
            'mean_R_X': 'R/X', 'mean_section': 'Section', 'diameter_length': 'Diam. Géom.',
            'diameter_Z': 'Diam. Imp.',
            'total_length': 'Longueur', 'leaf_nodes': 'Postes'}

    medoid_df = network_data.loc[(network_data.cluster_label == cluster_label) & network_data.medoid,
                                 network_data.columns].copy()
    medoid_df = medoid_df[list(cols.keys())].round(2)
    medoid_df.rename(columns=cols, inplace=True)

    table_spec = ' '.join(['l'] * len(cols))
    with doc.create(Table(position='H')) as table:
        table.add_caption('Caractéristique du réseau représentatif')
        with doc.create(Tabular(table_spec=table_spec, booktabs=True)) as tabular:
            tabular.add_row(medoid_df.columns)
            tabular.add_hline()
            tabular.add_row(medoid_df.iloc[0].values)


def fill_intro(doc, cluster_results, method, metric, n_clusters, voltage_level):
    """

    :param doc: the document
    """
    network_count = cluster_results.shape[0]
    network_length = round(cluster_results.total_length.sum(), 1)
    v_lev = 'HTA' if voltage_level == 'mv' else 'BT'
    intro = f"Les données géométriques, topologiques et électrotechniques (résistance, réactance et intensité maximale)" \
            f" correspondant à {network_count} réseaux {v_lev} ({network_length} kms de ligne)" \
            f" ont été traitées afin de calculer les paramètres descriptitfs de chacun des réseaux (détails "

    intro_2 = f"). Une méthode de classification ascendante hiérarchique ({method}, distance {metric}) a ensuite été appliquée" \
              f" afin de grouper les réseaux similaires du point de vue des paramètres descriptifs. Ci-dessous " \
              f"l'évolution de la distance des clusters groupés en fonction du nombre de cluster suivi du dendogramme" \
              f" résultant de la classification"

    intro_3 = f"Dans la suite, le choix a été fait de fixer le nombre de clusters à {n_clusters}. Pour chacun des " \
              f"clusters, le médoïde (réseau à distance moyenne la plus faible des autres membres du cluster) a été " \
              f"sélectionné pour représenter le cluster. Le chapitre suivant présente les caractéristiques des clusters" \
              f" et réseaux représentatifs retenus."

    distance_filename = plot_path / f"distance_merged_cluster_{voltage_level}_{method}_{metric}.pdf"
    dendogram_filename = plot_path / f"dendogram_{voltage_level}_{method}_{metric}.pdf"

    with doc.create(Chapter('Introduction')):

        doc.append(intro)
        doc.append(hyperlink("https://docs.google.com/document/d/1DKaAJz4qIcLuiFIfkBWHilH1x4iq6fuA8VgrPBTgUH8/edit",
                             "ici"))
        doc.append(intro_2)

        with doc.create(Figure(position='H')) as distance_pic:
            distance_pic.add_image(distance_filename.as_posix(), width='450px')
            distance_pic.add_caption('Distance des clusters groupés en fonction du nombre de cluster')

        with doc.create(Figure(position='H')) as dendogram_pic:
            dendogram_pic.add_image(dendogram_filename.as_posix(), width='450px')
            dendogram_pic.add_caption('Dendogramme résultant de la classification')

        doc.append(intro_3)


def create_doc():

    geometry_options = {"tmargin": "2.0cm", "bmargin": "2.0cm", "lmargin": "2.5cm", "rmargin": "3.cm"}
    doc = Document(documentclass='report',
                   page_numbers=True,
                   indent=True,
                   geometry_options=geometry_options,
                   )
    doc.packages.append(Package('hyperref'))
    doc.packages.append(Package('float'))
    doc.packages.add(Package('babel', options='french'))

    return doc


if __name__ == '__main__':

    cluster_result_folder = data_path / 'network' / 'cluster_results'

    method = 'ward'
    metric = 'euclidean'
    # voltage_level = 'mv'
    voltage_level = 'lv'
    n_clusters = 50
    cluster_results = pd.read_hdf(cluster_result_folder / f"{voltage_level}_network_clustered_{method}_{metric}.hdf")
    medoids = pd.read_csv(cluster_result_folder / f'{voltage_level}_network_medoids_{method}_{metric}.csv')

    mv_network_df = load_params(voltage_level)

    doc = create_doc()
    generate_header(doc)

    fill_title_page(doc, voltage_level)
    fill_intro(doc, cluster_results=cluster_results, method=method, metric=metric,
               n_clusters=n_clusters, voltage_level=voltage_level)
    fill_results(doc, n_clusters, medoids, cluster_results, voltage_level)
    doc.generate_pdf(report_path / f'clustering_report_{voltage_level}_{method}_{metric}_{n_clusters}', clean_tex=False)

    # # Add stuff to the document
    # with doc.create(Section('A second section')):
    #     doc.append('Some text.')
    #
    # doc.generate_pdf('basic_maketitle2', clean_tex=False)
    # tex = doc.dumps()  # The document as string in LaTeX syntax