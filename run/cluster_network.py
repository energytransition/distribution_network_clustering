import numpy as np
import pandas as pd
from pathlib import Path
import warnings
import os

warnings.filterwarnings("ignore")
data_path = Path.cwd().parent / 'data'
plot_path = Path.cwd().parent / 'plots'
from sklearn.preprocessing import StandardScaler
import scipy.cluster.hierarchy as shc
import scipy.spatial.distance as sd
import matplotlib.pyplot as plt
import fastcluster as fc
import seaborn as sns


def load_params(voltage_level):
    """

    :param voltage_level:
    :return:
    """
    print('loading data')
    input_folder = data_path / 'network' / 'network_params'
    network_df = pd.read_hdf(input_folder / f'{voltage_level}_network.hdf', key='all_data')

    return network_df


def scale_data(network_df):
    """

    :param network_df:
    :return:
    """

    print('scaling data')
    scaler = StandardScaler()
    return scaler.fit_transform(network_df)


def get_medoids(scaled_df):
    cols = scaled_df.columns
    scaled_df['medoid'] = False

    print('Getting medoids')

    for cluster_label in scaled_df.cluster_label.unique():
        ids = scaled_df.loc[scaled_df.cluster_label == cluster_label].index
        mean_dist = np.mean(sd.squareform(sd.pdist(scaled_df.loc[ids, cols])), axis=0)
        medoid_id = np.argmin(mean_dist)
        scaled_df.loc[ids[medoid_id], 'medoid'] = True

    return scaled_df


def cluster(scaled_network_data, network_df, n_clusters, voltage_level, method='ward', metric='euclidean',
            dendogram_p=150, max_cluster_elbow=500):
    print('Calculating linkage matrix')

    linkage_matrix = fc.linkage(scaled_network_data, method=method, metric=metric, preserve_input=False)
    plot_dendogram(linkage_matrix, dendogram_p, method, metric, voltage_level)
    plot_distance(linkage_matrix, max_cluster_elbow, method, metric, voltage_level)

    print('Getting flat clusters')

    cluster_labels = shc.fcluster(linkage_matrix, criterion='maxclust', t=n_clusters)
    scaled_df = pd.DataFrame(index=network_df.index, columns=network_df.columns, data=scaled_network_data)
    scaled_df['cluster_label'] = cluster_labels
    scaled_df = get_medoids(scaled_df)

    network_df['cluster_label'] = cluster_labels
    network_df['medoid'] = scaled_df['medoid']

    plot_cluster_distributions(scaled_df, method, metric, voltage_level)

    return linkage_matrix, network_df, scaled_df


def plot_cluster_distributions(scaled_df, method, metric, voltage_level):
    cols = ['aerial_share', 'mean_R_l', 'mean_imax', 'mean_R_X', 'mean_section', 'diameter_length', 'diameter_Z',
            'total_length', 'leaf_nodes']

    output_folder = plot_path / f"cluster_distribution_{voltage_level}_{method}_{metric}"
    os.makedirs(output_folder, exist_ok=True)

    print('plotting cluster distributions')

    for cluster_label in scaled_df.cluster_label.unique():
        fig_dims = (12, 5)
        fig, ax = plt.subplots(figsize=fig_dims)
        cluster_vals = scaled_df.loc[scaled_df.cluster_label == cluster_label, cols]
        sns.violinplot(x="variable", y="value", data=pd.melt(cluster_vals), ax=ax)
        plt.xticks(rotation=45)
        for i in range(len(cols)):
            medoid = scaled_df.loc[(scaled_df.cluster_label == cluster_label) & (scaled_df.medoid)]
            plt.hlines(medoid.loc[:, cols[i]], xmin=-0.5 + i, xmax=0.5 + i, color='r',
                       linestyles='dashed')

        plt.title(f"Distribution of parameters within cluster {cluster_label}, count {cluster_vals.shape[0]}")
        plt.tight_layout()
        plt.savefig(output_folder / f'cluster_{cluster_label}.pdf')


def plot_distance(linkage_matrix, max_cluster_elbow, method, metric, voltage_level):
    print('plotting distance')

    plt.figure(figsize=(12, 8))
    plt.plot(np.flip(linkage_matrix[-max_cluster_elbow:, 2]))
    plt.title(f"Distance between merged clusters {method} {metric}")
    plt.xlabel("Cluster Count")
    plt.ylabel("Distances")
    plt.savefig(plot_path / f"distance_merged_cluster_{voltage_level}_{method}_{metric}.pdf")


def plot_dendogram(linkage_matrix, dendogram_p, method, metric, voltage_level):
    print('plotting dendogram')

    plt.figure(figsize=(12, 8))
    dend = shc.dendrogram(linkage_matrix, truncate_mode='lastp', p=dendogram_p, distance_sort='ascending')
    plt.title(f"Dendrogram {method} {metric}")
    plt.xlabel("Samples")
    plt.ylabel("Distances")
    plt.savefig(plot_path / f"dendogram_{voltage_level}_{method}_{metric}.pdf")


if __name__ == '__main__':
    output_folder = data_path / 'network' / 'cluster_results'
    method = 'ward'
    metric = 'euclidean'
    # voltage_level = 'mv'
    voltage_level = 'lv'
    n_clusters = 50

    network_df = load_params(voltage_level)
    scaled_data = scale_data(network_df)
    linkage_matrix, network_df, scaled_df = cluster(scaled_data, network_df, n_clusters=n_clusters,
                                                    voltage_level=voltage_level, method=method, metric=metric,
                                                    dendogram_p=150, max_cluster_elbow=500)

    network_df.to_hdf(output_folder / f'{voltage_level}_network_clustered_{method}_{metric}.hdf', key='all_data',
                      mode='w')
    medoids = network_df.loc[network_df.medoid]
    medoids['substation_id'] = medoids.index
    medoids['substation_id'].to_csv(output_folder / f'{voltage_level}_network_medoids_{method}_{metric}.csv',
                                    index=False)
