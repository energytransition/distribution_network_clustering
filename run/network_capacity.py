import pandas as pd
import numpy as np
from dnpf import data_path
from pathlib import Path
from dnpf.io.read_network import read_network_hdf
from dnpf.simulation.powerflow import network_powerflow
from dnpf.plot.powerflow import plot_network_powerflow
from time import time
from tqdm.contrib.concurrent import process_map
from tqdm import tqdm

network_data_path = Path.cwd().parent / 'data' / 'network'
bdtopo_data_path = network_data_path / 'bdtopo_network'


def load_bdtopo_data():
    regions = ['32', '84']
    data = pd.concat([pd.read_hdf(bdtopo_data_path / f"region_{reg}.gpkg") for reg in regions], axis=0)
    data.clip(lower=5.e2, upper=1.e5, inplace=True)
    return data


def get_active_load(buses, bdtopo_data, min_power_by_area, max_power_by_area, step_size):
    power_by_area = np.arange(min_power_by_area, max_power_by_area, step_size)
    active_load = pd.DataFrame(index=buses.index, columns=range(len(power_by_area)), data=0.)

    lv_sub_list = buses.loc[buses.index.str.contains('MVLV')].index
    lv_sub_list = [lv_sub for lv_sub in lv_sub_list if lv_sub in bdtopo_data.index]
    lv_sub_area = bdtopo_data.loc[lv_sub_list].values
    lv_sub_area = lv_sub_area.reshape((len(lv_sub_list), 1))
    active_load.loc[lv_sub_list, :] = np.tile(lv_sub_area, (1, len(power_by_area)))
    active_load *= np.tile(power_by_area, (buses.shape[0], 1))

    return active_load


def get_capacity(results, network_data, substation, voltage_limits):
    feeder_capacity = {}
    min_voltage = substation['nominal_voltage'].values[0] * voltage_limits[0]
    max_voltage = substation['nominal_voltage'].values[0] * voltage_limits[1]

    for feeder_id, feeder_result in results.items():
        voltage_mask = (feeder_result['voltage'] < min_voltage) | (feeder_result['voltage'] > max_voltage)
        voltage_violations = voltage_mask.sum(axis=0)
        voltage_max_step = voltage_violations.loc[voltage_violations == 0].index[-1]

        apparent_flow = feeder_result['active_flow'] ** 2 + feeder_result['reactive_flow'] ** 2
        apparent_flow.loc[:, :] = np.sqrt(apparent_flow.values)
        lines = network_data['feeders'][feeder_id]['lines']
        max_power = (lines['Imax'] * substation['nominal_voltage'].values[0]).values.reshape((-1, 1))
        max_power_mask = (apparent_flow > np.tile(max_power, (1, apparent_flow.shape[1])))
        max_power_violations = max_power_mask.sum(axis=0)
        max_power_max_step = max_power_violations.loc[max_power_violations == 0].index[-1]

        max_step = min(max_power_max_step, voltage_max_step)

        feeder_capacity[feeder_id] = {
            'step': max_step,
            'active_load': feeder_result['active_load'].loc[:, max_step].sum()
        }

    return pd.DataFrame.from_dict(feeder_capacity, orient='index')


def run_powerflow_hdf(bdtopo_data, mv_network_path, voltage_limits=(0.95, 1.05), min_power_by_area=0.02,
                      max_power_by_area=5., step_size=0.02):
    network_data = read_network_hdf(mv_network_path)
    network_data['feeders'] = {feeder_id: feeder_data for feeder_id, feeder_data in network_data['feeders'].items()
                               if feeder_data['buses'].index.str.contains('MVLV').sum() > 2}

    substation = network_data['substation']
    substation['setpoint'] = 1.02

    if len(network_data['feeders']) == 0:
        return {
            'total_capacity': 0.,
            'substation_id': substation.index[0]
        }

    active_load = {}
    reactive_load = {}
    for feeder_id, feeder_data in network_data['feeders'].items():
        active_load_ = get_active_load(feeder_data['buses'], bdtopo_data, min_power_by_area,
                                       max_power_by_area, step_size)
        active_load[feeder_id] = active_load_
        reactive_load[feeder_id] = active_load_ * 0.2

    per_unit_scale = {
        'voltage': substation['nominal_voltage'].values[0],
        'power': 3.e6  # 3 MW
    }

    results = network_powerflow(network_data,
                                active_load,
                                reactive_load,
                                per_unit_scale,
                                precision=1e-8,
                                max_iteration=10)

    capacity = get_capacity(results, network_data, substation, voltage_limits)
    print(f"substation : {substation.index[0]}")
    print(f"total capacity : {round(capacity['active_load'].sum() / 1.e6, 2)} MW")

    return {
        'total_capacity': round(capacity['active_load'].sum() / 1.e6, 2),
        'substation_id': substation.index[0]
    }

    # for step in range(10):
    #     fig = plot_network_powerflow(network_data, results, step)
    #     fig.show()
    # pass


def wrapped_powerflow(input_dict):
    return run_powerflow_hdf(**input_dict)


if __name__ == '__main__':

    bdtopo_data = load_bdtopo_data()
    network_path = network_data_path / 'network_data' / 'mv_network'
    network_files = [file_path for file_path in Path(network_path).glob('*') if ('hdf' in file_path.name)]

    test_sub = 'HVMV440'
    network_files = [f for f in network_files if test_sub in f.name]

    inputs = []
    for net_file in network_files:
        inputs.append({
            'bdtopo_data': bdtopo_data,
            'mv_network_path': net_file
        })

    if len(inputs) > 10:
        capacities = process_map(wrapped_powerflow, inputs, max_workers=16, chunksize=1)
    else:
        capacities = []
        for inp in tqdm(inputs):
            capacities.append(wrapped_powerflow(inp))
