import pandas as pd
import os
from load_network_data import load_regions
from get_network_params import group_by_substation, process_network
from tqdm.contrib.concurrent import process_map, thread_map
from pathlib import Path
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from tqdm import tqdm

plot_path = Path.cwd().parent / 'plots'
data_path = Path.cwd().parent / 'data'


def run():

    input_folder = data_path / 'network' / 'cluster_results'
    method = 'ward'
    metric = 'euclidean'
    voltage_level = 'mv'

    network_data = load_regions(voltage_level, max_workers=10)
    # medoids = pd.read_csv(input_folder / f'{voltage_level}_network_medoids_{method}_{metric}.csv')

    sub_id_col = 'mv_lv_substation_id' if voltage_level == 'lv' else 'hv_mv_substation_id'

    # network_data['lines'] = network_data['lines'].loc[network_data['lines'][sub_id_col].isin(np.ravel(medoids.values))]

    grouped_network_data = group_by_substation(network_data['lines'],
                                               network_data['buses'],
                                               network_data['substations'],
                                               voltage_level)

    # tmp_ = process_network(grouped_network_data[0])
    processed_networks = process_map(process_network, grouped_network_data, max_workers=20, chunksize=1)

    output_folder = data_path / 'network' / 'network_data' / f'{voltage_level}_network'

    for i, processed_network in enumerate(processed_networks):
        network_params = processed_network[0]
        network_graph = processed_network[2]
        sub_id = network_params['sub_id']
        output_path = output_folder / f"network_{sub_id}.hdf"

        first_line = network_data['lines'].loc[network_data['lines'][sub_id_col] == sub_id].iloc[0]
        network_edges = nx.to_pandas_edgelist(network_graph)
        network_buses = pd.DataFrame.from_dict(dict(network_graph.nodes(data=True)), orient='index')
        network_buses['x'], network_buses['y'] = zip(*network_buses.position)
        network_buses.drop(columns=['position'], inplace=True)
        substation_position = grouped_network_data[i]['sub']['geometry'].iloc[0].xy
        feeders = network_edges.feeder_id.unique()

        with pd.HDFStore(output_path, mode='w', complib='blosc:lz4hc', complevel=9) as store:

            substation = pd.DataFrame(index=[sub_id],
                                      columns=['nominal_voltage', 'setpoint'],
                                      data=[[first_line['feeder_nominal_voltage'], first_line['mv_setpoint']]])

            store.put(key='substation', value=substation)

            for feeder_id in feeders:
                feeder_edges = network_edges.loc[network_edges.feeder_id == feeder_id]
                feeder_buses = network_buses.loc[network_buses.feeder_id == feeder_id]
                substation_bus = pd.DataFrame(index=[sub_id], columns=feeder_buses.columns,
                                              data=[[feeder_id, substation_position[0][0], substation_position[1][0]]])
                feeder_buses = pd.concat([substation_bus, feeder_buses], axis=0)
                store.put(key=f'feeders/{feeder_id}/buses', value=feeder_buses)
                store.put(key=f'feeders/{feeder_id}/lines', value=feeder_edges)


if __name__ == '__main__':

    run()
