import networkx as nx
import numpy as np
from load_network_data import load_regions
import pandas as pd
from pathlib import Path
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map, thread_map

data_path = Path.cwd().parent / 'data'



def select_lines_buses(sub, buses, lines, voltage_level):
    """
    Selects the lines and buses connected to a given substation
    :param sub:
    :param buses:
    :param lines:
    :param voltage_level:
    :return:
    """

    sub_id = sub['id']
    sub_id_col = 'mv_lv_substation_id' if voltage_level == 'lv' else 'hv_mv_substation_id'

    buses_in_network = buses.loc[buses[sub_id_col] == sub_id]
    lines_in_network = lines.loc[lines[sub_id_col] == sub_id]

    return buses_in_network, lines_in_network


def create_network_graph(sub, buses, lines):
    """
    Creates a NetworkX Graph instance with nodes being the buses of a network and the edges being the lines

    :param sub:
    :param buses:
    :param lines:
    :return:
    """
    network_graph = nx.Graph()

    xy = sub['geometry'].iloc[0].xy
    network_graph.add_nodes_from([sub['id'].iloc[0]], position=(xy[0][0], xy[1][0]))

    node_list = []

    for i, row in buses.iterrows():
        xy = row['geometry'].xy
        node_dict = {'position': (xy[0][0], xy[1][0]), 'feeder_id': row['feeder_id']}
        node_tuple = (row['id'], node_dict)
        node_list.append(node_tuple)

    network_graph.add_nodes_from(node_list)

    edge_list = []

    for i, row in lines.iterrows():
        edge_dict = {'R': row['R'], 'X': row['X'], 'Z': row['Z'],
                     'length': row['length'], 'Imax': row['Imax'], 'feeder_id': row['feeder_id']}
        edge_tuple = (row['bus1_id'], row['bus2_id'], edge_dict)
        edge_list.append(edge_tuple)

    network_graph.add_edges_from(edge_list)
    dfs_tree = nx.dfs_tree(network_graph, source=sub['id'].iloc[0])
    dfs_edges = list(dfs_tree.edges())
    network_edges = {(edge[0], edge[1]): edge[2] for edge in network_graph.edges(data=True)}

    dfs_network = nx.DiGraph()
    dfs_network.add_nodes_from(node_list)

    cnt = 0
    cnt_all = 0
    for dfs_edge in dfs_edges:
        reversed_edge = (dfs_edge[1], dfs_edge[0])
        if dfs_edge in network_edges.keys():
            dfs_network.add_edge(dfs_edge[0], dfs_edge[1], **network_edges[dfs_edge])
            cnt += 1
        else:
            dfs_network.add_edge(dfs_edge[0], dfs_edge[1], **network_edges[reversed_edge])
            cnt_all += 1

    return dfs_network, network_graph


def get_network_params(lines, network_graph):
    """
    Obtain the parameters needed for the clustering for each network
    :param lines:
    :param network_graph:
    :return:
    """

    param_dict = {}

    param_dict['aerial_share'] = lines.loc[lines.Aerial, 'length'].sum() / lines['length'].sum()
    param_dict['aerial_mean_length'] = lines.loc[lines.Aerial, 'length'].mean()
    param_dict['aerial_std_length'] = lines.loc[lines.Aerial, 'length'].std()
    param_dict['ug_mean_length'] = lines.loc[lines.Aerial == False, 'length'].mean()
    param_dict['ug_std_length'] = lines.loc[lines.Aerial == False, 'length'].std()
    param_dict['mean_imax'] = lines['Imax'].mean()
    param_dict['std_imax'] = lines['Imax'].std()
    param_dict['mean_R_l'] = lines['R_l'].mean()
    param_dict['std_R_l'] = lines['R_l'].std()
    param_dict['mean_X_l'] = lines['X_l'].mean()
    param_dict['std_X_l'] = lines['X_l'].std()
    param_dict['mean_R_X'] = (lines['R_l'] / lines['X_l']).mean()
    param_dict['std_R_X'] = (lines['R_l'] / lines['X_l']).std()
    param_dict['mean_section'] = lines['Section'].mean()
    param_dict['std_section'] = lines['Section'].std()
    param_dict['leaf_nodes'] = len([x for x in network_graph.nodes() if network_graph.degree(x) == 1])
    param_dict['total_length'] = lines['length'].sum()

    for measure in ['length', 'Z']:
        shortest = nx.shortest_path_length(network_graph, weight=measure)
        ecc = nx.eccentricity(network_graph, sp=dict(shortest))
        param_dict[f'diameter_{measure}'] = nx.diameter(network_graph, e=ecc)
        param_dict[f'closeness_{measure}'] = max(list(nx.closeness_centrality(network_graph,
                                                                              distance=measure).values()))

    return param_dict


def process_network(input_dict):
    """
    Creates NetworkX graph and get network params
    :return:

    """

    sub = input_dict['sub']
    buses = input_dict['buses']
    lines = input_dict['lines']

    dfs_network, network_graph = create_network_graph(sub, buses, lines)
    network_params = get_network_params(lines, network_graph)
    network_params['sub_id'] = sub['id'].iloc[0]

    return network_params, network_graph, dfs_network


def random_substation_selection(count, lines, voltage_level):
    """
    Select the lines that corresponds to a random selection of substations

    :param voltage_level: voltage_level, can be either 'mv' or 'lv
    :param count: number of substations to selec
    :param lines: GeoDataFrame containing line parameters
    :return: GeoDataFrame containing line parameters
    """
    sub_id_param = 'mv_lv_substation_id' if voltage_level == 'lv' else 'hv_mv_substation_id'

    all_sub_ids = lines[sub_id_param].unique()
    ids = np.random.choice(range(all_sub_ids.shape[0]), count, replace=False)
    selected_sub_ids = all_sub_ids[ids]
    selected_lv_lines = lines.loc[lines[sub_id_param].isin(selected_sub_ids)]

    return selected_lv_lines


def group_by_substation(lines, buses, substations, voltage_level):
    """
    Groups lines and buses by substation

    :param lines: GeoDataFrame containing line parameters
    :param buses: GeoDataFrame containing bus parameters
    :param substations: GeoDataFrame containing substation parameters
    :param voltage_level: voltage_level, can be either 'mv' or 'lv
    :return:
    """

    print('Grouping by substation')

    sub_id_param = 'mv_lv_substation_id' if voltage_level == 'lv' else 'hv_mv_substation_id'

    line_groups = lines.groupby(sub_id_param)
    bus_groups = buses.groupby(sub_id_param)
    inputs = []

    for name, line_group in tqdm(line_groups):
        sub = substations.loc[substations.id == name]
        bus_group = bus_groups.get_group(name)
        inputs.append({'sub': sub,
                       'buses': bus_group,
                       'lines': line_group})

    return inputs


def run(voltage_level, random_selection_count=None, max_workers=10):

    network_data = load_regions(voltage_level, max_workers=max_workers)

    if random_selection_count is not None:
        network_data['lines'] = random_substation_selection(random_selection_count,
                                                            network_data['lines'],
                                                            voltage_level)

    grouped_network_data = group_by_substation(network_data['lines'],
                                               network_data['buses'],
                                               network_data['substations'],
                                               voltage_level)

    print('Processing network data')
    chunksize = int(len(grouped_network_data) / max_workers) if len(grouped_network_data) > 1000 else 1
    network_params = process_map(process_network, grouped_network_data, max_workers=max_workers, chunksize=chunksize)

    network_df = pd.DataFrame.from_dict({par[0]['sub_id']: par[0] for par in network_params}, orient='index')
    network_df.fillna(0., inplace=True)
    network_df.drop(columns=['sub_id'], inplace=True)

    return network_df


if __name__ == '__main__':

    output_folder = data_path / 'network' / 'network_params'

    # voltage_level = 'mv'
    voltage_level = 'lv'

    random_selection_count = None if voltage_level == 'mv' else int(2e4)

    mv_network_df = run(voltage_level, random_selection_count=random_selection_count, max_workers=20)
    mv_network_df.to_hdf(output_folder / f'{voltage_level}_network.hdf', key='all_data', mode='w')

