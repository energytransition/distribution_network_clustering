import fiona
import geopandas as gpd

gpd.options.use_pygeos = True
from pathlib import Path
from multiprocessing import Pool
import warnings
import time

warnings.filterwarnings("ignore")
from tqdm import tqdm
import pandas as pd
import os
import shutil
import numpy as np

data_path = Path.cwd().parent / 'data'

bdtopo_path = Path('/media/yassine/code_data/bdtopo')


def load_mv_lv_subs(file_path):
    print('Loading network')
    mv_lv_sub = gpd.read_file(file_path, layer='mv_lv_substations')
    mv_lv_sub = mv_lv_sub.set_crs("EPSG:4326")
    mv_lv_sub = mv_lv_sub.to_crs("EPSG:2154")
    return mv_lv_sub


def load_bdtopo_region(region, subs):

    print('Loading BDTOPO')
    district_bounds = subs.geometry.unary_union.convex_hull.bounds
    bdtopo_region_path = bdtopo_path / f"BDTOPO_R{region}.gpkg"

    features = []
    with fiona.open(bdtopo_region_path, 'r', layer='batiment') as src:
        for f in src.filter(bbox=district_bounds):
            features.append(f)

    if src.crs is not None:
        crs = src.crs
    else:
        crs = 'EPSG:2154'

    bdtopo_gdf = gpd.GeoDataFrame.from_features(features, crs=crs)
    usage_list = ['Résidentiel', 'Agricole', 'Commercial et services']
    bdtopo_gdf = bdtopo_gdf.loc[bdtopo_gdf.usage_1.isin(usage_list)]
    bdtopo_gdf.nombre_d_etages = bdtopo_gdf.nombre_d_etages.fillna(1.)
    area_gdf = gpd.GeoDataFrame(geometry=bdtopo_gdf.geometry.centroid, data=bdtopo_gdf.geometry.area * bdtopo_gdf.nombre_d_etages)
    return area_gdf


if __name__ == '__main__':

    # regions = [('32', 'pas_de_calais.gpkg'), ('84', 'aura.gpkg')]
    regions = [('84', 'aura.gpkg')]

    for region, network_file in regions:
        print(f'running region {region}')
        network_data_path = data_path / 'network' / 'roseau_inputs' / network_file
        mv_lv_subs = load_mv_lv_subs(network_data_path)

        bdtopo = load_bdtopo_region(region, mv_lv_subs)
        print('Joining bdtopo and network')
        bdtopo_with_sub = bdtopo.sjoin_nearest(mv_lv_subs, how='left', distance_col='mv_lv_distance', max_distance=5.e4)
        surface_by_sub = bdtopo_with_sub.groupby('id')[0].sum()

        output_path = data_path / 'network' / 'bdtopo_network' / f'region_{region}.hdf'

        with pd.HDFStore(output_path, mode='w', complib='blosc:lz4hc', complevel=9) as store:
            store.put(key='all_data', value=surface_by_sub)
    pass
