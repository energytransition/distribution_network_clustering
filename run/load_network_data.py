import geopandas as gpd
import numpy as np
import pandas as pd
from pathlib import Path
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map, thread_map

data_path = Path.cwd().parent / 'data'

line_rename_dict = {
    'r': 'R_l',
    'x': 'X_l',
    'maximal_current': 'Imax',
}


def load_network_data(file_path, layers):
    """
    reads the layers of a GIS file and returns them in a dict with layer names as keys

    :param file_path: path to the file (str or Path instance)
    :param layers: list of layer names to read
    :return: dict of GeoDataFrame
    """

    return {layer: gpd.read_file(file_path, layer=layer) for layer in layers}


def set_line_params(lines, line_dict):
    """
    Sets the total resistance, reactance and impedance as well as the section and aerial properties of the lines
    :param lines: GeoDataFrame containing line parameters
    :param line_dict: dict whose keys are types of lines and values their section and aerial property
    :return: GeoDataFrame containing line parameters
    """
    for type_n, values in line_dict.items():
        mask = lines.type_name == type_n
        for key, val in values.items():
            lines.loc[mask, key] = val

    lines['R'] = lines['length'] * lines['R_l']
    lines['X'] = lines['length'] * lines['X_l']
    lines['Z'] = np.sqrt(lines['R'] ** 2 + lines['X'] ** 2)

    return lines


def load_mv_network(file_path):
    """
    Load medium voltage lines in a given GeoPackage file

    :param file_path: path to GeoPackage file containing network data
    :return: dict of GeoDataFrames containing network data
    """

    layers = ['hv_mv_substations', 'mv_lines', 'mv_buses', 'mv_lv_substations']
    mv_line_dict = {
        'A_AM_148': {'Aerial': True, 'Section': 148.},
        'S_AL_150': {'Aerial': False, 'Section': 150.},
        'A_AM_54': {'Aerial': True, 'Section': 54.},
    }

    mv_network = load_network_data(file_path, layers)
    mv_network['mv_lines'].rename(columns=line_rename_dict, inplace=True)
    mv_network['mv_buses'] = pd.concat([mv_network['mv_buses'], mv_network['mv_lv_substations']], axis=0)
    mv_network['mv_lines'] = set_line_params(mv_network['mv_lines'], mv_line_dict)

    return mv_network


def load_lv_network(file_path):
    """
    Load low voltage lines in a given GeoPackage file

    :param file_path: path to GeoPackage file containing network data
    :return: dict of GeoDataFrames containing network data
    """

    layers = ['mv_lv_substations', 'lv_lines', 'lv_buses']
    lv_line_dict = {
        'S_AL_150': {'Aerial': False, 'Section': 150.},
        'T_AL_70': {'Aerial': True, 'Section': 70.},
    }

    lv_network = load_network_data(file_path, layers)
    lv_network['lv_lines'].rename(columns=line_rename_dict, inplace=True)
    lv_network['lv_lines'] = set_line_params(lv_network['lv_lines'], lv_line_dict)

    return lv_network


def load_regions(voltage_level, max_workers=10):
    """
    Load network data for several regions by iterating over the GeoPackages present in the input folder

    :param voltage_level: voltage_level, can be either 'mv' or 'lv
    :return: GeoDataFrames containing network data
    """

    input_folder = data_path / 'network'/ 'roseau_inputs'
    input_files = [file_path for file_path in Path(input_folder).glob('*') if 'gpkg' in file_path.name]

    print('Loading network data')
    if voltage_level == 'lv':
        network_by_region = process_map(load_lv_network, input_files, max_workers=max_workers)
    else:
        network_by_region = process_map(load_mv_network, input_files, max_workers=max_workers)

    sub_layer = 'mv_lv_substations' if voltage_level == 'lv' else 'hv_mv_substations'

    return {
        'lines': pd.concat([nr[f"{voltage_level}_lines"] for nr in network_by_region], axis=0),
        'buses': pd.concat([nr[f"{voltage_level}_buses"] for nr in network_by_region], axis=0),
        'substations': pd.concat([nr[sub_layer] for nr in network_by_region], axis=0),
    }

