import pandas as pd
import os
from load_network_data import load_regions
from get_network_params import group_by_substation, process_network
from tqdm.contrib.concurrent import process_map, thread_map
from pathlib import Path
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from tqdm import tqdm

plot_path = Path.cwd().parent / 'plots'
data_path = Path.cwd().parent / 'data'

if __name__ == '__main__':

    input_folder = data_path / 'network' / 'cluster_results'
    method = 'ward'
    metric = 'euclidean'
    # voltage_level = 'mv'
    voltage_level = 'lv'

    network_data = load_regions(voltage_level, max_workers=10)
    medoids = pd.read_csv(input_folder / f'{voltage_level}_network_medoids_{method}_{metric}.csv')

    sub_id_col = 'mv_lv_substation_id' if voltage_level == 'lv' else 'hv_mv_substation_id'

    network_data['lines'] = network_data['lines'].loc[network_data['lines'][sub_id_col].isin(np.ravel(medoids.values))]

    grouped_network_data = group_by_substation(network_data['lines'],
                                               network_data['buses'],
                                               network_data['substations'],
                                               voltage_level)

    print('Processing network data')
    network_params = process_map(process_network, grouped_network_data, max_workers=20, chunksize=1)

    output_folder = plot_path / f"cluster_graph_{voltage_level}_{method}_{metric}"
    os.makedirs(output_folder, exist_ok=True)
    print('plotting network graph')
    for network_param in tqdm(network_params):
        network_graph = network_param[1]
        network_id = network_param[0]['sub_id']
        fig_dims = (10, 10)
        fig, ax = plt.subplots(figsize=fig_dims)
        node_list = list(network_graph.nodes)
        sizes = list(map(lambda x: 10 if x != network_id else 300, node_list))
        colors = list(map(lambda x: 'blue' if x != network_id else 'red', node_list))
        nx.draw_networkx(network_graph,
                         pos=nx.get_node_attributes(network_graph, 'position'),
                         with_labels=False,
                         node_size=sizes,
                         node_color=colors)
        plt.savefig(output_folder / f"graph_{voltage_level}_{network_id}.pdf")
        plt.close(fig)
